// fetch api return promise
fetch('todos/luigi.json').then((response) => {
  console.log('resolved', response);
  // const data = response.json(); // will not work
  return response.json();
}).then(data => {
  console.log(data);
  return fetch('todos/mario.json');
})
.then(response => response.json())
.then(data => console.log(data))
.catch((err) => console.log(err));

// fetch('todos/luigi.json')
// .then((response) => response.json())
// .then(data => {
//   console.log(data);
//   return fetch('todos/mario.json');
// })
// .then(response => response.json())
// .then(data => {
//   console.log(data);
//   return fetch('todos/shaun.json');
// })
// .then(response => response.json())
// .then(data => {
//   console.log(data);
// })
// .catch((err) => console.log(err));