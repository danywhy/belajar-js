const getTodos = (url, callback) => {
  const request = new XMLHttpRequest();

  request.addEventListener('readystatechange', () => {
    // console.log(request, request.readyState);
    if (request.readyState === 4 && request.status === 200) {
      // console.log(request, request.responseText);
      // callback(undefined, request.responseText);
      const data = JSON.parse(request.responseText);
      callback(undefined, data);
    } else if (request.readyState === 4) {
      // console.log('could not fetch the data');
      callback('could not fetch the data', undefined);
    }
  });

  request.open('GET', url);
  request.send();
};

// callback hell

getTodos('todos/luigi.json', (err, data) => {
  console.log(data);

  getTodos('todos/mario.json', (err, data) => {
    console.log(data);

    getTodos('todos/shaun.json', (err, data) => {
      console.log(data);
    });
  });
});



