// promise example
const getSomething = () => {

  // promise resolve or reject
  return new Promise((resolve, reject) => {
    // fetch something
    resolve('some data');
    // reject('some error');
  });
};

getSomething().then((data) => {
  console.log(data);
}, (err) => {
  console.log(err);
});

getSomething().then(data => {
  console.log(data);
}).catch(err => {
  console.log(err);
});


const getTodos = (url) => {

  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();

    request.addEventListener('readystatechange', () => {
      // console.log(request, request.readyState);
      if (request.readyState === 4 && request.status === 200) {
        // console.log(request, request.responseText);
        // callback(undefined, request.responseText);
        const data = JSON.parse(request.responseText);
        resolve(data);
      } else if (request.readyState === 4) {
        // console.log('could not fetch the data');
        reject('error getting resource');
      }
    });

    request.open('GET', url);
    request.send();
  });
  
};


getTodos('todos/luigis.json').then(data => {
  console.log(data);
}).catch(err => {
  console.log(err);
})