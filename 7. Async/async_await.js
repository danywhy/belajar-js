// membuat fungsi untuk fetch
const getTodos = async () => {

  const response1 = await fetch('todos/luigi.json');
  // console.log(response);
  const data1 = await response1.json();
  console.log(data1);

  const response2 = await fetch('todos/mario.json');
  const data2 = await response2.json();
  console.log(data2);

  const response3 = await fetch('todos/shaun.json');
  const data3 = await response3.json();
  console.log(data3);

};

// test promise
// const test = getTodos();
// console.log(test);

// console.log(1);

// getTodos();

// console.log(2);
// console.log(3);
// console.log(4);