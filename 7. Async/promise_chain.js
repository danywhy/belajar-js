// chaining promise
const getTodos = (url) => {

  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();

    request.addEventListener('readystatechange', () => {
      if (request.readyState === 4 && request.status === 200) {
        const data = JSON.parse(request.responseText);
        resolve(data);
      } else if (request.readyState === 4) {
        reject('error getting resource');
      }
    });

    request.open('GET', url);
    request.send();
  });
  
};

getTodos('todos/luigi.json').then(data => {
  console.log('promise 1', data);
  return getTodos('todos/mario.json');
}).then(data => {
  console.log('promise 2: ', data);
  return getTodos('todos/shaun.json');
}).then(data => {
  console.log('promise 3:', data);
}).catch(err => {
  console.log(err);
})