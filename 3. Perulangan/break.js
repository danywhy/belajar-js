// break & continue
const scores = [50, 55, 40, 0, 70, 85, 100, 90, 65];
for (let i = 0; i < scores.length; i++) {

  if (scores[i] === 0) {
    continue;
  }
  console.log('your score: ', scores[i]);

  if (scores[i] === 100) {
    console.log('congrats, you got the top score');
    break;
  }
}


