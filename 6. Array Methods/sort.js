// SORT METHOD
// example 1
const names = ['dani', 'yuni', 'mario', 'atha', 'aidan'];
names.sort();
console.log(names);

// example 2
// scores.sort();
scores.sort((a, b) => a - b);
console.log(scores);

const scoreGames = [
  {player: 'mario', score: 50},
  {player: 'yoshi', score: 30},
  {player: 'mario', score: 70},
  {player: 'crystal', score: 60},

  // {player: 'mario', score: 50},
  // {player: 'yoshi', score: 30},
  // {player: 'mario', score: 70},
  // {player: 'crystal', score: 60},

  // {player: 'mario', score: 50},
  // {player: 'yoshi', score: 30},
  // {player: 'mario', score: 70},
  // {player: 'crystal', score: 60},

  // {player: 'mario', score: 50},
  // {player: 'yoshi', score: 30},
  // {player: 'mario', score: 70},
  // {player: 'crystal', score: 60},
];

scoreGames.sort((a, b) => {
  return a.score - b.score;
});
console.log(scoreGames);