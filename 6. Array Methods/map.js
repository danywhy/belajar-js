// MAP METHOD
const prices = [100000, 200000, 150000, 400000, 250000, 500000, 50000];

const discountPrices = prices.map(price => {
  return price - price * 0.05;
});
console.log(discountPrices);

const products =  [
  {name: 'Samsung', price: 300},
  {name: 'Iphone', price: 500},
  {name: 'Oppo', price: 200},
  {name: 'Xiaomi', price: 250},
  {name: 'Advance', price: 100}
];

const saleProducts = products.map(product => {
  if (product.price > 250) {
    // destructive, mengubah nilai array utama
    // product.price = product.price / 2;
    // return product;
    return {name: product.name, price: product.price / 2};
  } else {
    return product;
  }
});
console.log(saleProducts, products);




