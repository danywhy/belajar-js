// REDUCE METHOD
const scores = [50, 55, 40, 0, 70, 85, 100, 90, 65];

const result = scores.reduce((acc, curr) => {
  if (curr > 70) {
    acc++;
  }
  return acc;
}, 0);
console.log(result);

const scoreGames = [
  {player: 'mario', score: 50},
  {player: 'yoshi', score: 30},
  {player: 'mario', score: 70},
  {player: 'crystal', score: 60},

  // {player: 'mario', score: 50},
  // {player: 'yoshi', score: 30},
  // {player: 'mario', score: 70},
  // {player: 'crystal', score: 60},

  // {player: 'mario', score: 50},
  // {player: 'yoshi', score: 30},
  // {player: 'mario', score: 70},
  // {player: 'crystal', score: 60},

  // {player: 'mario', score: 50},
  // {player: 'yoshi', score: 30},
  // {player: 'mario', score: 70},
  // {player: 'crystal', score: 60},
];

const marioTotal = scoreGames.reduce((acc, curr) => {
  if (curr.player === 'mario') {
    acc += curr.score;
  }
  return acc;
}, 0);
console.log(marioTotal);