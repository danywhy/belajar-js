const scores = [50, 55, 40, 0, 70, 85, 100, 90, 65];

// FILTER METHOD
const filteredScores = scores.filter((score) => {
  return score > 60;
});

console.log(scores);
console.log(filteredScores);

const users = [
  {name: 'Dani', premium: true},
  {name: 'Upin', premium: false},
  {name: 'Ipin', premium: false},
  {name: 'Mail', premium: true},
];

const premiumUsers = users.filter(user => {
  return user.premium;
});

console.log(premiumUsers);



