// CHAINING ARRAY METHODs
const products =  [
  {name: 'Samsung', price: 300},
  {name: 'Iphone', price: 500},
  {name: 'Oppo', price: 200},
  {name: 'Xiaomi', price: 250},
  {name: 'Advance', price: 100}
];

const filtered = products.filter(product => product.price > 250);
console.log(filtered);
const promo = filtered.map(product => product.price - product.price * 0.05);
// console.log(promo);

const promos = products
                .filter(product => product.price > 250)
                .map(product => product.price - product.price * 0.05);

// console.log(promos);

// HOW TO KEEP OBJECT STRUCTURE WHEN USE MAP
const promos2 = products
                .filter(product => product.price > 250)
                .map(product => {
                  let price = product.price - product.price * 0.05;
                  return {name: product.name, price: price};
                });
console.log(promos2);

