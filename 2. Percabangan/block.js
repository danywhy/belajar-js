// variables & block scope

let umur = 30;
// let umur = 40;

if (true) {
  // umur = 40;
  let umur = 40;
  let nama = 'dany';
  console.log('di dalam code block: ', umur, nama);

  if (true) {
    let umur = 50;
    console.log('di dalam code block kedua: ', umur);
    var test = 'hello';
  }
}

console.log('di luar code block: ', umur, test);