let umur = 25;
let flag = true;

// loose comparison (diffrent types can still be equal)

if (umur == '25') {  // == 25 | == '25' | != 25 | != '25' |  
  flag = true;
} else {
  flag = false;
}



// strict comparison (diffrent types cannot be equal)
if (umur !== '25') {  // === 25 | === '25' | !== 25 | !== '25' |  
  flag = true;
} else {
  flag = false;
}



console.log(flag);