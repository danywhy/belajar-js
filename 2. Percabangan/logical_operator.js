let password = 'p@ssword';

// logical operator
if (password.length >= 12 && password.includes('@')) {
  console.log('password anda cukup panjang dan kuat');
} else if (password.length >= 8 || password.includes('@') && password.length > 5) {
  console.log('password anda cukup kuat');
} else {
  console.log('password anda tidak cukup kuat');
}

// logical NOT (!)
let isLogin = false;

if (!isLogin) {
  console.log('Anda belum login');
}



