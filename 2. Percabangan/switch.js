const grade = 'A';

// using switch statement
switch(grade) {
  case 'A':
    console.log('Anda mendapatkan nilai A');
    break;
  case 'B':
    console.log('Anda mendapatkan nilai B');
    break;
  case 'C':
    console.log('Anda mendapatkan nilai C');
    break;
  case 'D':
    console.log('Anda mendapatkan nilai D');
    break;
  case 'E':
    console.log('Anda mendapatkan nilai E');
    break;
  default: 
    console.log('not valid');
}



// using if statement
if (grade === 'A') {

} else if (grade === 'B') {

} else if (grade === 'C') {

} else if (grade === 'D') {

} else if (grade === 'E') {

} else {

}