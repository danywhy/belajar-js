function User(username, email) {
  this.username = username;
  this.email = email;

  // this.login = function() {
  //   console.log(`${this.username} has logged in`);
  // }
}

User.prototype.login = function() {
  console.log(`${this.username} has logged in`);
  return this;
}

User.prototype.logout = function() {
  console.log(`${this.username} has logged out`);
  return this;
}

const userOne = new User('Dani', 'dani@gmail.com');
const userTwo = new User('Yuni', 'yuni@gmail.com');

console.log(userOne, userTwo);
userOne.login();
userOne.logout();

userTwo.login().logout();