// constructor function

// class User {
  
//   constructor(username, email) {
//     // set up properties 
//     this.username = username;
//     this.email = email;
//     this.score = 0;
//   }
// }

function User(username, email) {
  this.username = username;
  this.email = email;

  this.login = function() {
    console.log(`${this.username} has logged in`);
  }
}

const userOne = new User('Dani', 'dani@gmail.com');
const userTwo = new User('Yuni', 'yuni@gmail.com');

console.log(userOne, userTwo);
userOne.login();