const userOne = {
  username: 'Dani',
  email: 'dani@gmail.com',
  login() {
    console.log('the user logged in');
  },
  logout() {
    console.log('the user logged out');
  }
};

console.log(userOne.username, userOne.email);
userOne.login();

const userTwo = {
  username: 'Yuni',
  email: 'yuni@gmail.com',
  login() {
    console.log('the user logged in');
  },
  logout() {
    console.log('the user logged out');
  }
};

console.log(userTwo.username, userTwo.email);
userTwo.login();

// bila ingin membuat userThree maka kita akan membuat dari ulang lagi
// tidak efektif, akan lebih efektif menggunakan class