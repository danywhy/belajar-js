class User {
  
  constructor(username, email) {
    // set up properties 
    this.username = username;
    this.email = email;
    this.score = 0;
  }

  login() {
    console.log(`${this.username} just logged in`);
    return this;
  }

  logout() {
    console.log(`${this.username} just logged out`);
    return this;
  }

  inScore() {
    this.score++;
    console.log(`${this.username} has a score of ${this.score}`);
    return this;
  }
}

class Admin extends User {
  deleteUser(user) {
    users = users.filter(u => {
      return u.username !== user.username;
    });
  }
}

const userOne = new User('Dani', 'dani@gmail.com');
const userTwo = new User('Yuni', 'yuni@gmail.com');

const userAdmin = new Admin('Admin', 'admin@gmail.com');
console.log(userAdmin);

let users = [userOne, userTwo, userAdmin];
console.log(users);

userAdmin.deleteUser(userTwo);
console.log(users);

userOne.deleteUser(userAdmin);