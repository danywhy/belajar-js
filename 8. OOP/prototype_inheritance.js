function User(username, email) {
  this.username = username;
  this.email = email;
}

User.prototype.login = function() {
  console.log(`${this.username} has logged in`);
  return this;
}

User.prototype.logout = function() {
  console.log(`${this.username} has logged out`);
  return this;
}

function Admin(username, email, position) {
  User.call(this, username, email);
  this.position = position;
}

Admin.prototype = Object.create(User.prototype);

Admin.prototype.deleteUser = function() {
  // delete user
}

const userOne = new User('Dani', 'dani@gmail.com');
const userTwo = new User('Yuni', 'yuni@gmail.com');
const userAdmin = new Admin('Admin', 'admin@gmail.com', 'Manager');

console.log(userOne, userTwo);
console.log(userAdmin);