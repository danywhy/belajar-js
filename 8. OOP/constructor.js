class User {
  
  constructor(username, email) {
    // set up properties 
    this.username = username;
    this.email = email;
  }
}

const userOne = new User('Dani', 'dani@gmail.com');
console.log(userOne);
const userTwo = new User('Yuni', 'yuni@gmail.com');
console.log(userTwo);

// the 'new' keyword
// 1 - it creates a new empty object {}
// 2 - it binds the value of 'this' to the new empty object
// 3 - it calls the constructor function to 'build' the object