// ADDING METHODS
let user = {
  name: 'Dani',
  age: 30,
  email: 'dany.sambuari@gmail.com',
  location: 'yogya',
  blogs: ['Javascript', 'CSS', 'HTML'],
  login: function() {
    console.log('the user logged in');
  },
  logout: function() {
    console.log('the user logged out');
  },
  // this keyword
  // logBlogs: function() {
    // console.log(blogs); // error

    // use this => this is context object represent the user object
    // console.log(this);
    // console.log(this.blogs);
    // this.blogs.forEach(blog => {
      // console.log(blog);
    // })
  // shortcut function
  logBlogs() { // cara lain mendefinisikan method
    this.blogs.forEach(blog => {
      console.log(blog);
    })
  }
};
user.login();

user.logout();
user.logBlogs();

console.log(this);





