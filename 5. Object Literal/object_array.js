// OBJECT IN ARRAY
const blogs = [
  { title: 'Javascript', likes: 30},
  { title: 'CSS', likes: 20},
  { title: 'HTML', likes: 50}
]
console.log(blogs);

let user = {
  name: 'Dani',
  age: 30,
  email: 'dany.sambuari@gmail.com',
  location: 'yogya',
  blogs: blogs,
  login() {
    console.log('the user logged in');
  },
  logout() {
    console.log('the user logged out');
  },
  logBlogs() {
    this.blogs.forEach(blog => {
      console.log(blog.title, blog.likes);
    })
  }
};
user.logBlogs();