// Object Literal
let user = {
  name: 'Dani',
  age: 30,
  email: 'dany.sambuari@gmail.com',
  location: 'yogya',
  blogs: ['Javascript', 'CSS', 'HTML']
};

console.log(user);
console.log(user.name);

user.age = 35;
console.log(user.age);

console.log(user['name']);
console.log(user['email']);

user['name'] = 'yuni';
console.log(user['name']);

const key = 'location';
console.log(user[key]);

console.log(typeof user);


