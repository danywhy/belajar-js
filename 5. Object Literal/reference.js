// primitive values
let scoreOne = 50; // immutable
// saat dicopy js akan membuat variable yang benar-benar terpisah
let scoreTwo = scoreOne; 

console.log(`scoreOne: ${scoreOne}, scoreTwo: ${scoreTwo}`);

scoreOne = 100;

console.log(`scoreOne: ${scoreOne}, scoreTwo: ${scoreTwo}`);


// reference values
userOne = {name: 'Dani', age: 30}; // mutable
// saat dicopy js tidak akan membuat value baru
// tapi mereferensikan value variable pertama ke variable kedua
// artinya kedua variable akan memegang value yang sama
// jika value diubah, maka semua variable value nya berubah
userTwo = userOne;

console.log(userOne, userTwo);

userOne.name = 'Yuni';

console.log(userOne, userTwo);

