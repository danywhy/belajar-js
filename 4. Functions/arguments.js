// ARGUMENTS & PARAMETERS
const speak = function(name) {  // value yang dilewatkan adalah parameters
  console.log(`good day! ${name}`);
};
speak('dany'); // value yang dilewatkan adalah arguments

const speak2 = function(name, time) {  // value yang dilewatkan adalah parameters
  console.log(`good ${time}! ${name}`);
};
speak2('dany', 'morning'); // value yang dilewatkan adalah arguments

// DEFAULT VALUES
const speak3 = function(name = 'dany', time = 'night') {
  console.log(`good ${time}! ${name}`);
};
speak3(); 
speak3('andi', 'afternoon');

// RETURNING VALUES
const calcArea = function(radius) {
  let area = 3.14 * radius**2;
  return 3.14 * radius**2;
};

const result = calcArea(5);
console.log(result);

const total = function(area) {
  return area + area;
};

const totalResult = total(result);
console.log(totalResult);


