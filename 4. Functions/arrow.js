// ARROW FUNCTIONS 1 PARAMETER
const calcArea2 = radius => {
  return 3.14 * radius**2;
};
const result2 = calcArea2(4);
console.log(result2);

// ARROW FUNCTIONS 1 PARAMETER 1 SINGLE STATEMENT
const calcArea3 = radius => 3.14 * radius**2;
const result3 = calcArea3(4);
console.log(result3);

// ARROW FUNCTIONS 2 PARAMETER ATAU TANPA PARAMETER
const multiplication = (a, b) => {
  return a * b;
}
const result4 = multiplication(5, 3);
console.log(result4);

// PRACTICE ARROW FUNCTION
const greet = function() {
  return 'hello, world';
}

const greet2 = () => 'hello, world';
const result5 = greet2();
console.log(result5);

// MENGGUNAKAN FUNGSI BIASA
const bill = function(products, tax) {
  let total = 0;
  for (let i = 0; i < products.length; i++) {
    total += products[i] + products[i] * tax;
  }
  return total;
}
console.log(bill([10, 15, 30], 0.2));

// MENGGUNAKAN ARROW FUNCTION
const bill2 = (products, tax) => {
  let total = 0;
  for (let i = 0; i < products.length; i++) {
    total += products[i] + products[i] * tax;
  }
  return total;
}
console.log(bill2([10, 15, 30], 0.2));