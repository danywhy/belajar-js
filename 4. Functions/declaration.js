// FUNCTION DECLARATION
function greet() {
  console.log('hello there');
}
greet();

// FUNCTION EXPRESSION
const speak = function() {
  console.log('good day!');
};

speak();

// JavaScript Hoisting
showMe();
function showMe() {
  console.log("it's me");
}
showMe();

display();
const display = function() {
  console.log('display function');
}
display();

