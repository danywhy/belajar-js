// CALLBACK FUNCTIONS & FOREACH
const myFunct = (callbackFunc) => {
  let value = 50;
  callbackFunc(value);
}

myFunct(function(value) {
  console.log(value);
});

myFunct(value => console.log(value));

const people = ['Upin', 'Ipin', 'Ehsan', 'Mei Mei', 'Susanti', 'Jarjit', 'Fizi', 'Mail'];

people.forEach(function(person) {
  console.log('something');
  console.log(person);
});

people.forEach(person => console.log(person));

people.forEach((person, index) => {
  console.log(person, index);
});

const logPerson = (person, index) => {
  console.log(`${index} - hello ${person}`);
}

people.forEach(logPerson);


// CALBACK FUNCTION IN ACTION
const ul = document.querySelector('.people');
let html = '';

people.forEach(person => {
  // create html template
  html += `<li style="color: purple">${person}</li>`
});
ul.innerHTML = html;