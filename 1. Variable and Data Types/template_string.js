const title = 'paling terkenal 2021';
const author = 'alip ba ta';
const subscriber = 5.13;


// concatenation way
let result = 'Gitaris youtube ' + title + ' yaitu ' + author + ' mempunyai ' + subscriber + ' juta subscriber';
console.log(result);


// template string / template literal way
let result = `Gitaris youtube ${title} yaitu ${author} mempunyai ${subscriber} juta subscriber`;
console.log(result);

// creating html templates
let html = `
  <h2>${title}</h2>
  <p>${author}</p>
  <span>youtuber ini mempunyai ${subscriber} juta subscriber</span>
`;
console.log(html);