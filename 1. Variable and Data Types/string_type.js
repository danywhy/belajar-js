// strings
console.log('hello, world');

let email = 'javascript@gmail.com';
console.log(email);

// string concatenation
let firstName = 'Dany';
let lastName = 'Sambuari'
let fullName = firstName + ' ' + lastName;

console.log(fullName);

// getting characters
console.log(fullName[2]);

// string length
console.log(fullName.length);

// string methods
console.log(fullName.toUpperCase());
let result = fullName.toLowerCase();
console.log(result);
console.log(fullName);

let index = email.indexOf('@');
console.log(index);


