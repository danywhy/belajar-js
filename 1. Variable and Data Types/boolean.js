// console.log(true, false, "true", "false");

// methods can return booleans
let email = 'javascript@gmail.com';
let result = email.includes('@');
console.log(result);


// comparison operators
let umur = 25;

console.log(umur == 25);
console.log(umur == 30);
console.log(umur != 25);
console.log(umur > 20);
console.log(umur < 20);
console.log(umur <= 25);
console.log(umur >= 25);

let name = 'dany';

console.log(name == 'dany');
console.log(name == 'Dany');


