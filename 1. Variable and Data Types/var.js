let age = 25;
let year = 2019;

// log value to the console
console.log(age, year);

/* comment multiple line
age = 30;
console.log(age);
*/

const points = 100;

console.log(points);

var score = 75;
console.log(score);

// string
let name = 'dani';
console.log(name);

// array
const arr = ['dani', 'yuni', 'aidan', 20, 40];
console.log(arr);

// https://www.w3schools.com/js/js_reserved.asp

